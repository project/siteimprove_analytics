# Siteimprove Analytics

## Configuration
Add your Siteimprove Analytics code by going to the settings under path: admin/config/system/siteimprove-analytics.
It is even better to add the code to your settings.php white the following line:
    
    $config['siteimprove_analytics.settings']['code'] = XXXXXXX;

## Good to know
The Siteimprove Analytics script will only be added if the code can be found, the user is anonymous and the path 
does not match the following values: "/admin*\n/batch*\n/node/add*\n/node/*/edit\n/node/*/delete\n/user/*/edit*\n/user/*/cancel*"

