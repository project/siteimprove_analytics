<?php

namespace Drupal\siteimprove_analytics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm
 *
 * @package Drupal\siteimprove_analytics\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'siteimprove_analytics_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['siteimprove_analytics.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('siteimprove_analytics.settings');

    $form['code'] = [
      '#title' => $this->t('Code'),
      '#default_value' => $config->get('code'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('siteimprove_analytics.settings');
    $config
      ->set('code', $form_state->getValue('code'))
      ->save();
  }

}
